CREATE OR REPLACE PROCEDURE p_wall_board IS
  CURSOR c_servicios IS
    SELECT s.nume_serv
          ,s.nume_base
          ,s.nume_esta_serv estado_del_servicio
          ,CASE
             WHEN nume_tipo_serv = 1 THEN
              'Mec�nica'
             WHEN nume_tipo_serv = 2 THEN
              'Traslados'
           END tipo_servicio
          ,CASE
             WHEN nume_tipo_serv = 1 THEN
              1
           END mecanica
          ,CASE
             WHEN nume_tipo_serv = 2 THEN
              1
           END traslados
          ,CASE
             WHEN nume_cier = 100 AND nume_esta_serv >= 60 OR (nume_cier IN (6, 113, 15, 11, 4, 5, 12, 17, 3, 14, 13, 24, 10, 26, 111, 2, 28, 18)) THEN
              '1'
           END finalizados
          ,CASE
             WHEN nume_cier IN (6, 113, 15, 11, 4, 5, 12, 17, 3, 14, 13, 24, 10, 26, 111, 2, 28) THEN
              1
           END anulados
          ,CASE
             WHEN flag_recl > 0 THEN
              1
             ELSE
              0
           END servicios_reclamados
      FROM servicios s
     WHERE nume_serv >= to_char(trunc(SYSDATE), 'yyyymmdd') || 100000
       AND nume_base IN (6001, 10541, 8700, 5052); --trafico;
  v_demora_real          t_wall_board.demora_real%TYPE;
  v_demoras              t_wall_board.demoras%TYPE;
  v_cantidad_finalizados t_wall_board.cantidad_finalizados%TYPE;
  v_count                NUMBER(4);
  v_error                VARCHAR2(500);
  v_demora_real_60_min   t_wall_board.demora_60_min%TYPE;
  v_cant                 NUMBER(2);
BEGIN
  DELETE FROM t_wall_board
   WHERE nro_servicio IS NOT NULL;
  FOR r IN c_servicios LOOP
    SAVEPOINT p_save;
    BEGIN
      v_cant                 := NULL;
      v_demora_real          := NULL;
      v_demoras              := NULL;
      v_demora_real_60_min   := NULL;
      v_cantidad_finalizados := NULL;
      v_error                := NULL;
      v_count                := NULL;
      IF r.anulados IS NULL THEN
        SELECT COUNT(1)
          INTO v_demoras
          FROM servicioshorarios
         WHERE nume_serv = r.nume_serv
           AND hora_pedi <= ((SYSDATE - (120 / 24) / 60))
           AND hora_lleg IS NULL
           AND hora_prog IS NULL;
        BEGIN
          SELECT nvl(trunc((hora_lleg - hora_pedi) * 24 * (60)), 0) demora_real
            INTO v_demora_real
            FROM servicioshorarios
           WHERE nume_serv = r.nume_serv
             AND hora_lleg IS NOT NULL
             AND hora_fina >= ((SYSDATE - (60 / 24) / 60))
             AND hora_prog IS NULL;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
        SELECT COUNT(1)
          INTO v_count
          FROM servicioshorarios
         WHERE nume_serv = r.nume_serv
           AND hora_prog IS NULL
           AND 1 = (SELECT COUNT(1)
                      FROM servicios
                     WHERE nume_esta_serv < 60
                       AND nume_cier = 100
                       AND nume_serv = r.nume_serv);
      
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        v_error := SQLERRM;
    END;
    <<finalizar>>
    BEGIN
      INSERT INTO dba_sosdw.t_wall_board
        (nro_servicio
        ,base
        ,tipo_servicio
        ,cantidad_mecanica
        ,cantidad_traslados
        ,demora_real
        ,cantidad_finalizados
        ,cantidad_anulados
        ,demoras -- demoras mas de 3 horas que aun no han cerrado
        ,servicio_reclamado -- en curso
        ,log_error)
      VALUES
        (r.nume_serv
        ,r.nume_base
        ,r.tipo_servicio
        ,r.mecanica
        ,r.traslados
        ,CASE WHEN v_demora_real > 0 THEN v_demora_real END
        ,r.finalizados
        ,r.anulados 
        ,v_demoras
        ,v_count
        ,v_error);
    EXCEPTION
      WHEN OTHERS THEN
        v_error := 'Error al insertar los valores para el registro ' || r.nume_serv || ' ' || SQLERRM;
        ROLLBACK TO p_save;
    END;
    COMMIT;
  END LOOP;
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    v_error := 'Error al ejecutar p_wall_board ' || SQLERRM;
END p_wall_board;
/
